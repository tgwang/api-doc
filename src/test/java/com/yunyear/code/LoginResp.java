package com.yunyear.code;

import java.util.List;

/**
 * Created by tgwang on 2017/5/3.
 */
public class LoginResp {
    private SysUser user;
    private List<SysMenu> menus;

    public LoginResp() {
    }

    public LoginResp(SysUser user, List<SysMenu> menus) {
        this.user = user;
        this.menus = menus;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    public List<SysMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenu> menus) {
        this.menus = menus;
    }

    @Override
    public String toString() {
        return "LoginResp{" +
                "user=" + user +
                ", menus=" + menus +
                '}';
    }
}
