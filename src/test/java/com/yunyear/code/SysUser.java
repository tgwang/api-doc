package com.yunyear.code;

import com.yunyear.code.apidoc.annotations.ApiDocParam;
import com.yunyear.code.apidoc.model.ApiType;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by tgwang on 2017/4/21.
 */
public class SysUser implements Serializable {
    private static final long serialVersionUID = -9204685886813793596L;

    @ApiDocParam(apiType = ApiType.RESP, param = "userId", type = "字符串", range = "1-16", require = true, desc = "ceshi", objId = "Menu", objModel = SysMenu.class)
    private Long userId;

    @ApiDocParam(apiType = ApiType.REQ, param = "account", type = "字符串", range = "1-16", require = true, desc = "account11")
    private String account;
    private String password;
    private String salt;
    private String realname;
    private Integer sex;
    private String phone;
    private String email;
    private Date createdTime;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "userId=" + userId +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", realname='" + realname + '\'' +
                ", sex='" + sex + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", createdTime=" + createdTime +
                '}';
    }
}
