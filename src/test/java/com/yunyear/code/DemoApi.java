package com.yunyear.code;


import com.yunyear.code.apidoc.annotations.ApiDoc;
import com.yunyear.code.apidoc.annotations.ApiDocCode;
import com.yunyear.code.apidoc.annotations.ApiDocParam;
import com.yunyear.code.apidoc.model.ApiMethod;
import com.yunyear.code.apidoc.model.ApiType;

/**
 * Created by lenovo on 2017/8/15.
 */
//@ApiDoc(name = "Demo组", api = "demo", method = {ApiMethod.FUNCTION}, desc = "用于显示demo信息的组", history = {"【2017-06-07】新建项目"})
@ApiDocParam(apiType = ApiType.REQ, param = "authAccount", type = "字符串", range = "1-16", require = false, desc = "鉴权账号")
@ApiDocParam(apiType = ApiType.REQ, param = "authPassword", type = "字符串", range = "1-16", require = false, desc = "鉴权密码")
@ApiDocParam(apiType = ApiType.RESP, param = "retCode", type = "字符串", range = "6", require = true, desc = "返回码")
@ApiDocParam(apiType = ApiType.RESP, param = "retMsg", type = "字符串", range = "-", require = true, desc = "返回描述")
@ApiDocCode(code = "000000", msg = "成功")
@ApiDocCode(code = "999000", msg = "失败")
public class DemoApi {

    @ApiDoc(name = "获取API接口", api = "list", method = {ApiMethod.HTTP_GET}, desc = "这个是一个测试的用例", history = {"【2017-08-01】增加返回密码逻辑", "【2017-08-08】优化性能"})
    @ApiDocParam(apiType = ApiType.REQ, param = "name", type = "字符串", range = "1-16", require = false, desc = "这个参数只是测试<br>测试程序&lt")
    @ApiDocParam(apiType = ApiType.REQ, param = "password", type = "字符串", range = "1-16", require = false, desc = "这个参数只是测试1")
    @ApiDocParam(apiType = ApiType.REQ, param = "sex", type = "字符串", range = "1", require = false, desc = "这个参数只是测试2")
    @ApiDocParam(apiType = ApiType.RESP, param = "user", type = "-", range = "1", require = true, desc = "用户信息", objId = "User", objModel = SysUser.class)
    @ApiDocParam(apiType = ApiType.RESP, param = "name", type = "字符串", range = "1-16", require = true, desc = "用户名", refObjId = "User")
    @ApiDocParam(apiType = ApiType.RESP, param = "password", type = "字符串", range = "8-32", require = true, desc = "密码", refObjId = "User", opTime = "2017-08-19", opName = "王天广", opRemark = "网站需要此信息，目的是显示密码")
    @ApiDocCode(code = "040001", msg = "鉴权失败")
    public String getApiDemo(String name) {
        return "";
    }

    @ApiDoc(name = "测试", api = "test", method = {ApiMethod.HTTP_GET, ApiMethod.HTTP_POST, ApiMethod.ALL}, desc = "测试类中的数据")
    public String getApiDemoInfo(String name) {
        return "";
    }
}
