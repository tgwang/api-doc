package com.yunyear.code;

import com.yunyear.code.apidoc.annotations.ApiDocParam;
import com.yunyear.code.apidoc.model.ApiParamType;
import com.yunyear.code.apidoc.model.ApiType;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by tgwang on 2017/4/21.
 */
public class SysMenu implements Serializable {
    private static final long serialVersionUID = -9204685886813793596L;

    @ApiDocParam(apiType = ApiType.REQ, param = "menuId", type = ApiParamType.NUMBER, range = "1-16", require = true, desc = "menuId11")
    private Long menuId;

    @ApiDocParam(apiType = ApiType.REQ, param = "menuName", type = ApiParamType.STRING, range = "1-16", require = true, desc = "menuName11")
    private String menuName;
    private String parentMenuId;
    private String parentsMenuId;
    private Long menuOrder;
    private String menuUrl;
    private String menuIcon;
    private Date createdTime;

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getParentMenuId() {
        return parentMenuId;
    }

    public void setParentMenuId(String parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    public String getParentsMenuId() {
        return parentsMenuId;
    }

    public void setParentsMenuId(String parentsMenuId) {
        this.parentsMenuId = parentsMenuId;
    }

    public Long getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(Long menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        SysMenu sysMenu = (SysMenu) o;
//
//        return menuId != null ? menuId.equals(sysMenu.menuId) : sysMenu.menuId == null;
//    }
//
//    @Override
//    public int hashCode() {
//        return menuId != null ? menuId.hashCode() : 0;
//    }

    @Override
    public String toString() {
        return "SysMenu{" +
                "menuId=" + menuId +
                ", menuName='" + menuName + '\'' +
                ", parentMenuId='" + parentMenuId + '\'' +
                ", parentsMenuId='" + parentsMenuId + '\'' +
                ", menuOrder='" + menuOrder + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", menuIcon='" + menuIcon + '\'' +
                ", createdTime=" + createdTime +
                '}';
    }
}
