package com.yunyear.code.apidoc;

import com.google.gson.GsonBuilder;
import com.yunyear.code.apidoc.model.*;
import com.yunyear.code.apidoc.utils.ClassUtils;
import com.yunyear.code.apidoc.utils.StringUtils;
import com.yunyear.code.apidoc.annotations.ApiDoc;
import com.yunyear.code.apidoc.annotations.ApiDocCode;
import com.yunyear.code.apidoc.annotations.ApiDocParam;
import com.yunyear.code.apidoc.exceptions.ApiTypeException;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by lenovo on 2017/8/15.
 */
public class ApiDocHandler {
    public static ApiDocModule getAppiDocModule(String pack, String moduleId, String moduleName, String moduleDesc) throws ApiTypeException {
        Set<Class<?>> classes = ClassUtils.getClasses(pack);

        ApiDocModule module = new ApiDocModule();
        module.setModule(moduleId.replace(" ", ""));
        module.setName(moduleName);
        module.setDesc(moduleDesc);

        List<ApiDocDetail> apiDocMethods = new ArrayList<>();
        for (Class<?> cla : classes) {
            // 获取每个类的公共返回码，放到各个方法中的返回码中
            ApiDocCode[] classApiDocCodesAnno = cla.getAnnotationsByType(ApiDocCode.class);

            List<ApiDocCodeDetail> classApiDocCodes = new ArrayList<>();
            if (null != classApiDocCodesAnno) {
                classApiDocCodes.addAll(getApiDocCodeDetails(classApiDocCodesAnno));
            }

            ApiDocParam[] classApiDocParams = cla.getAnnotationsByType(ApiDocParam.class);

            List<ApiDocParamDetail> classApiDocReqParams = new ArrayList<>();
            List<ApiDocParamDetail> classApiDocRespParams = new ArrayList<>();
            if (null != classApiDocParams) {
                try {
                    getApiDocParamDetails(classApiDocParams, classApiDocReqParams, classApiDocRespParams);
                } catch (ApiTypeException e) {
                    throw new ApiTypeException(MessageFormat.format("类定位：{0}，异常原因：{1}", cla.getCanonicalName(), e.getMessage()));
                }
            }

            // 获取每个类中的Doc信息
            ApiDoc classApiDocAnno = cla.getAnnotation(ApiDoc.class);
            ApiDocSummary classApiDocSummary = new ApiDocSummary();

            if (null != classApiDocAnno) {
                classApiDocSummary.setApi(classApiDocAnno.api());
                classApiDocSummary.setName(classApiDocAnno.name());
                classApiDocSummary.setDesc(classApiDocAnno.desc());
                String[] apiMethodsAnno = classApiDocAnno.method();

                List<String> apiMethods = new ArrayList<>();

                if (null != apiMethodsAnno) {
                    for (String m : apiMethodsAnno) {
                        apiMethods.add(m);
                    }
                }

                classApiDocSummary.setMethod(apiMethods);
                classApiDocSummary.setHistory(Arrays.asList(classApiDocAnno.history()));
            }


            Method[] methods = cla.getMethods(); // 查询所有公有方法

            if (null == methods) {
                continue;
            }

            // 获取方法中的API信息
            for (Method method : methods) {
//                System.out.println("方法：" + method.getName());
                ApiDocDetail apiDocMethod = new ApiDocDetail();

                // 获取Api的信息
                ApiDoc apiDoc = method.getAnnotation(ApiDoc.class);

                if (null == apiDoc) {
                    continue;
                }

                // 获取相关信息

                String api = StringUtils.defaultIfBlank(classApiDocSummary.getApi(), "") + "/" + apiDoc.api(); // 无论有多少/，先拼接成整体在进行过滤
                api = api.replaceAll("/+", "/");
                api = api.startsWith("/") ? api : "/" + api;
//                api = api.endsWith("/") ? api.substring(0, api.length() - 1) : api;   // 如果写成 /a/b/ 形式，不应该去掉后面的/

                apiDocMethod.setApi(api);
                apiDocMethod.setName(apiDoc.name());
                apiDocMethod.setDesc(!StringUtils.isBlank(classApiDocSummary.getDesc()) ? "【" + classApiDocSummary.getDesc() + "】<br/> " + apiDoc.desc() : apiDoc.desc());
                apiDocMethod.setGroup(classApiDocSummary.getApi());
                apiDocMethod.setGroupName(classApiDocSummary.getName());


                // 获取方法的 method 信息
                List<String> methodMethods = new ArrayList<>();
                if (null != classApiDocSummary.getMethod()) {
                    methodMethods.addAll(classApiDocSummary.getMethod());
                }

                if (null != apiDoc.method()) {
                    for (String m : apiDoc.method()) {
                        methodMethods.add(m);
                    }
                }
                apiDocMethod.setMethod(methodMethods);

                // 获取方法的 history 信息
                List<String> his = new ArrayList<>();
                if (null != classApiDocSummary.getHistory()) {
                    his.addAll(classApiDocSummary.getHistory());
                }
                his.addAll(Arrays.asList(apiDoc.history()));

                apiDocMethod.setHistory(his);

                // 获取参数信息

                List<ApiDocParamDetail> apiDocReqParams = new ArrayList<>();
                List<ApiDocParamDetail> apiDocRespParams = new ArrayList<>();

                // 将类上的参数绑定到各个方法中
                apiDocReqParams.addAll(classApiDocReqParams);
                apiDocRespParams.addAll(classApiDocRespParams);

                ApiDocParam[] apiDocParams = method.getAnnotationsByType(ApiDocParam.class);
                if (null != apiDocParams) {
                    try {
                        getApiDocParamDetails(apiDocParams, apiDocReqParams, apiDocRespParams);
                    } catch (ApiTypeException e) {
                        throw new ApiTypeException(MessageFormat.format("方法定位：{0}，异常原因：{1}", cla.getCanonicalName() + "." + method.getName(), e.getMessage()));
                    }
                }

                apiDocMethod.setReq(apiDocReqParams);
                apiDocMethod.setResp(apiDocRespParams);

                // 获取返回码
                ApiDocCode[] apiDocCodes = method.getAnnotationsByType(ApiDocCode.class);

                List<ApiDocCodeDetail> codes = new ArrayList<>();

                // 将类中的返回码添加到方法中
                codes.addAll(classApiDocCodes);

                if (null != apiDocCodes) {
                    codes.addAll(getApiDocCodeDetails(apiDocCodes));
                }

                apiDocMethod.setCodes(codes);

                apiDocMethods.add(apiDocMethod);
            }
        }

        apiDocMethods.sort((a, b) -> a.getApi().compareToIgnoreCase(b.getApi()));
        module.setDocs(apiDocMethods);

        return module;
    }

    /**
     * 获取返回码列表
     *
     * @param apiDocCodes ApiDocCode注解信息
     * @return
     */
    private static List<ApiDocCodeDetail> getApiDocCodeDetails(ApiDocCode[] apiDocCodes) {
        List<ApiDocCodeDetail> codes = new ArrayList<>();

        for (ApiDocCode apiDocCode : apiDocCodes) {
            ApiDocCodeDetail code = new ApiDocCodeDetail();
            code.setCode(apiDocCode.code());
            code.setMsg(apiDocCode.msg());

            codes.add(code);
        }

        return codes;
    }

    /**
     * @param apiDocParams     ApiDocParam注解数据列表
     * @param apiDocReqParams  req请求参数列表
     * @param apiDocRespParams resp请求参数列表
     */
    private static void getApiDocParamDetails(ApiDocParam[] apiDocParams, List<ApiDocParamDetail> apiDocReqParams, List<ApiDocParamDetail> apiDocRespParams) throws ApiTypeException {
        for (ApiDocParam apiDocParam : apiDocParams) {
            ApiDocParamDetail apiDocParamDetail = getApiDocParamDetail(apiDocParam);

            if (ApiType.REQ == apiDocParam.apiType()) {
                apiDocReqParams.add(apiDocParamDetail);
            } else if (ApiType.RESP == apiDocParam.apiType()) {
                apiDocRespParams.add(apiDocParamDetail);
            } else if (ApiType.NONE == apiDocParam.apiType()) {
                throw new ApiTypeException("定义ApiDocParam时需要指定 apiType");
            }

            // 获取modelClass的属性
            if (!StringUtils.isBlank(apiDocParam.objId()) && apiDocParam.objModel() != Void.class) {
                List<ApiDocParamDetail> fieldApiDocParamDetails = getFieldApiDocParams(apiDocParam.objModel(), apiDocParam.objId(), apiDocParam.apiType());

                // 类中的字段是
                if (ApiType.REQ == apiDocParam.apiType()) {
                    apiDocReqParams.addAll(fieldApiDocParamDetails);
                } else if (ApiType.RESP == apiDocParam.apiType()) {
                    apiDocRespParams.addAll(fieldApiDocParamDetails);
                }
            }
        }
    }

    /**
     * 获取param详情
     *
     * @param apiDocParam ApiDocParam注解数据
     * @return
     */
    private static ApiDocParamDetail getApiDocParamDetail(ApiDocParam apiDocParam) {
        return getApiDocParamDetail(apiDocParam, null);
    }

    /**
     * 获取param详情信息
     *
     * @param apiDocParam     ApiDocParam注解数据
     * @param specialRefObjId 特殊的RefObjId，如果设置，将使用此RefObjId
     * @return
     */
    private static ApiDocParamDetail getApiDocParamDetail(ApiDocParam apiDocParam, String specialRefObjId) {
        ApiDocParamDetail apiDocParamDetail = new ApiDocParamDetail();

        apiDocParamDetail.setParam(apiDocParam.param());
        apiDocParamDetail.setType(apiDocParam.type());
        apiDocParamDetail.setRange(apiDocParam.range());
        apiDocParamDetail.setRequire(apiDocParam.require());
        apiDocParamDetail.setDesc(apiDocParam.desc());
        apiDocParamDetail.setOpTime(apiDocParam.opTime());
        apiDocParamDetail.setOpName(apiDocParam.opName());
        apiDocParamDetail.setOpRemark(apiDocParam.opRemark());
        apiDocParamDetail.setObjId(apiDocParam.objId());
        apiDocParamDetail.setRefObjId(StringUtils.isBlank(specialRefObjId) ? apiDocParam.refObjId() : specialRefObjId);

        return apiDocParamDetail;
    }

    /**
     * 递归获取类中的字段
     *
     * @param cls      类
     * @param refObjId 注解引用对象ID
     * @return 类Field参数列表
     */
    private static List<ApiDocParamDetail> getFieldApiDocParams(Class cls, String refObjId, ApiType apiType) {
        List<ApiDocParamDetail> fieldApiDocParams = new ArrayList<>();

        getFieldApiDocParams(cls, refObjId, apiType, fieldApiDocParams);

        return fieldApiDocParams;
    }

    /**
     * 递归获取类中的字段
     *
     * @param cls               类
     * @param refObjId          注解引用对象ID
     * @param fieldApiDocParams 类Field参数列表
     */
    private static void getFieldApiDocParams(Class cls, final String refObjId, ApiType apiType, final List<ApiDocParamDetail> fieldApiDocParams) {
        Field[] fields = cls.getDeclaredFields();

        if (null == fields) {
            return;
        }

        Arrays.stream(fields).forEachOrdered(field -> {
            ApiDocParam[] apiDocParams = field.getAnnotationsByType(ApiDocParam.class);

            if (null == apiDocParams || apiDocParams.length == 0) {
                return;
            }

            // 需要过滤apiType
            List<ApiDocParam> realApiDocParams = Arrays.stream(apiDocParams).filter(param -> param.apiType() == apiType || param.apiType() == ApiType.NONE).collect(Collectors.toList());

            if (null == realApiDocParams || realApiDocParams.size() == 0) {
                return;
            }

            ApiDocParam realApiDocParam = realApiDocParams.get(realApiDocParams.size() - 1);

            ApiDocParamDetail detail = getApiDocParamDetail(realApiDocParam, refObjId);
            fieldApiDocParams.add(detail);

            // 前提是必须有objId
            if (!StringUtils.isBlank(realApiDocParam.objId()) && realApiDocParam.objModel() != Void.class) {

                getFieldApiDocParams(realApiDocParam.objModel(), realApiDocParam.objId(), apiType, fieldApiDocParams);
            }
        });
    }

    /**
     * 获取指定模块的API DOC
     *
     * @param destFileDir 目标文件夹路径
     * @param extName     文件扩展名
     * @param pack        包名
     * @param moduleId    模块标识（会当作文件名称）
     * @param moduleName  模块名称
     * @param moduleDesc  模块描述
     * @return 是否生成成功
     * @throws Exception 生成异常
     */
    public static boolean genApiDocModule(String destFileDir, String extName, String pack, String moduleId, String moduleName, String moduleDesc) throws Exception {
        CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
        Files.deleteIfExists(Paths.get(destFileDir, moduleId.replace(" ", "-").concat(extName)));

        OutputStream out = Files.newOutputStream(Paths.get(destFileDir, moduleId.replace(" ", "-").concat(extName)), StandardOpenOption.CREATE);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, encoder))) {
            ApiDocModule module = getAppiDocModule(pack, moduleId, moduleName, moduleDesc);
            String prettyJson = new GsonBuilder().setPrettyPrinting().create().toJson(module);
            writer.write(prettyJson);
        } catch (Exception e) {
            System.out.println("API文档生成异常，" + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 获取指定模块的API DOC
     *
     * @param destFileDir 目标文件夹路径
     * @param pack        包名
     * @param moduleId    模块标识（会当作文件名称）
     * @param moduleName  模块名称
     * @param moduleDesc  模块描述
     * @return 是否生成成功
     * @throws Exception 生成异常
     */
    public static boolean genApiDocModule(String destFileDir, String pack, String moduleId, String moduleName, String moduleDesc) throws Exception {
        return genApiDocModule(destFileDir, ".json", pack, moduleId, moduleName, moduleDesc);
    }
}
