package com.yunyear.code.apidoc.utils;

/**
 * Created by lenovo on 2017/8/18.
 */
public class StringUtils {
    /**
     * 字符串是否是空
     *
     * @param str 字符串
     * @return 是否为空
     */
    public static boolean isBlank(String str) {
        if (null == str) return true;
        if (str.trim().length() == 0) return true;

        return false;
    }

    /**
     * 默认值
     *
     * @param str 目标字符串
     * @param defaultStr 默认字符串
     * @return 最终字符串
     */
    public static String defaultIfBlank(String str, String defaultStr) {
        if (isBlank(str)) {
            return defaultStr;
        }

        return str;
    }
}
