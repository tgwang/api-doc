package com.yunyear.code.apidoc.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by lenovo on 2017/8/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ApiDoc {
    String name();  // 接口名称

    String[] method() default {};   // 调用方法

    String api();   // 接口地址

    String desc() default "";   // 接口描述

    String[] history() default {};  // 修改历史
}
