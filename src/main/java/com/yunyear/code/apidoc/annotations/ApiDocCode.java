package com.yunyear.code.apidoc.annotations;

import java.lang.annotation.*;

/**
 * Created by lenovo on 2017/8/17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Repeatable(ApiDocCodes.class)
public @interface ApiDocCode {

    String code(); // 响应码

    String msg(); // 响应码描述
}
