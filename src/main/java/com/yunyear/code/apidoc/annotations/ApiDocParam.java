package com.yunyear.code.apidoc.annotations;

import com.yunyear.code.apidoc.model.ApiType;

import java.lang.annotation.*;

/**
 * Created by lenovo on 2017/8/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Repeatable(ApiDocParams.class)
public @interface ApiDocParam {

    ApiType apiType() default ApiType.NONE; // apiType 类型，只有类中的Field才可以为空，类或者方法中的不可以未空，否则报错

    String param();     // 参数名称

    String type();      // 参数类型

    String range();    // 参数长度范围

    boolean require();    // 参数似乎否必填

    String desc();      // 参数描述

    String opTime() default "";      // 增加时间

    String opName() default "";      // 增加人姓名

    String opRemark() default "";     // 增加人备注

    String objId() default "";     // 对象ID，如果指定，使用此信息作为类名

    String refObjId() default "";     // 引用ID，用于对象下子元素

    Class objModel() default Void.class;      // 模型对象类，如果加上此属性会导致查看接口协议不再明显，需要再考虑
}
