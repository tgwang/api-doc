package com.yunyear.code.apidoc.exceptions;

/**
 * Created by lenovo on 2017/8/21.
 */
public class ApiTypeException extends Exception {
    public ApiTypeException() {
        super();
    }

    public ApiTypeException(String message) {
        super(message);
    }

    public ApiTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApiTypeException(Throwable cause) {
        super(cause);
    }

    protected ApiTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
