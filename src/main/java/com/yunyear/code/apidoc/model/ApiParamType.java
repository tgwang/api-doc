package com.yunyear.code.apidoc.model;

/**
 * Created by lenovo on 2017/8/21.
 */
public class ApiParamType {
    /**
     * 字符串
     */
    public final static String STRING = "字符串";

    /**
     * 字符串数组
     */
    public final static String STRING_ARRAY = "字符串数组";

    /**
     * 数字
     */
    public final static String NUMBER = "数字";

    /**
     * 数字数组
     */
    public final static String NUMBER_ARRAY = "数字数组";

    /**
     * 对象
     */
    public final static String OBJECT = "对象";

    /**
     * 对象数组
     */
    public final static String OBJECT_ARRAY = "对象数组";

    /**
     * 数组
     */
    public final static String ARRAY = "数组";
}
