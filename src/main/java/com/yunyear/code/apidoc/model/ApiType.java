package com.yunyear.code.apidoc.model;

/**
 * Created by lenovo on 2017/8/15.
 */
public enum ApiType {
    /**
     * 请求
     */
    REQ,

    /**
     * 响应
     */
    RESP,

    /**
     * 未指定
     */
    NONE
}
