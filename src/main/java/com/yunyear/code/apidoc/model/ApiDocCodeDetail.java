package com.yunyear.code.apidoc.model;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/8/18.
 */
public class ApiDocCodeDetail implements Serializable {
    private String code;    // 响应码
    private String msg;     // 响应描述

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ApiDocCodeDetail{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
