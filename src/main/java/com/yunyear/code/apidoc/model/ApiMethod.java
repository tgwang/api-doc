package com.yunyear.code.apidoc.model;

/**
 * Created by lenovo on 2017/8/19.
 */
public class ApiMethod {
    public final static String HTTP_GET = "GET请求";
    public final static String HTTP_POST = "POST请求";
    public final static String HTTP_PUT = "PUT请求";
    public final static String HTTP_PATCH = "PATCH请求";
    public final static String HTTP_DELETE = "DELETE请求";
    public final static String HTTP_HEAD = "HEAD请求";
    public final static String HTTP_OPTIONS = "OPTIONS请求";
    public final static String HTTP_TRACE = "TRACE请求";
    public final static String HTTP_CONNECT = "CONNECT请求";

    public final static String SOAP = "SOAP协议";   // SOAP协议，webservice协议

    public final static String FUNCTION = "函数调用";   // 函数调用
    public final static String RPC = "RPC调用";   // 远程RPC调用

    public final static String NONE = "无";
    public final static String ALL = "所有";// 所有
    public final static String UNKNOWN = "未知"; // 未知
    public final static String UNDEFINED = "未定义";   // 未定义
}
