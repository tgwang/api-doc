package com.yunyear.code.apidoc.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lenovo on 2017/8/18.
 */
public class ApiDocModule implements Serializable {
    private transient String module;      // 模块名称
    private String name;        // 模块名称
    private String desc;        // 模块描述
    private List<ApiDocCodeDetail> codes;  // 公共返回码
    private List<ApiDocDetail> docs;        // API文档

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ApiDocCodeDetail> getCodes() {
        return codes;
    }

    public void setCodes(List<ApiDocCodeDetail> codes) {
        this.codes = codes;
    }

    public List<ApiDocDetail> getDocs() {
        return docs;
    }

    public void setDocs(List<ApiDocDetail> docs) {
        this.docs = docs;
    }

    @Override
    public String toString() {
        return "ApiDocModule{" +
                "module='" + module + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", codes=" + codes +
                ", docs=" + docs +
                '}';
    }
}
