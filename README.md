# API文档生成器

使用注解维护API文档

## 样例

引入maven依赖
```xml
<dependency>
    <groupId>com.yunyear.code</groupId>
    <artifactId>api-doc</artifactId>
    <version>1.0.0</version>
</dependency>
```

示例代码
```java
package com.yunyear.demo.java8.anno.demo;


import com.yunyear.code.apidoc.annotations.ApiDoc;
import com.yunyear.code.apidoc.annotations.ApiDocCode;
import com.yunyear.code.apidoc.annotations.ApiDocParam;
import com.yunyear.code.apidoc.model.ApiMethod;
import com.yunyear.code.apidoc.model.ApiParamType;
import com.yunyear.code.apidoc.model.ApiType;

@ApiDoc(name = "鉴权类", api = "auth", desc = "用于账户鉴权相关的API")
@ApiDocParam(apiType = ApiType.RESP, param = "code", type = ApiParamType.NUMBER, range = "1-10", require = true, desc = "返回码")
@ApiDocParam(apiType = ApiType.RESP, param = "reason", type = ApiParamType.STRING, range = "-", require = true, desc = "返回描述")
@ApiDocCode(code = "200", msg = "成功")
@ApiDocCode(code = "500", msg = "失败")
public class AuthController {

    @ApiDoc(name = "登陆", api = "login", method = {ApiMethod.HTTP_POST}, desc = "用户登陆操作")
    @ApiDocParam(apiType = ApiType.REQ, param = "account", type = ApiParamType.STRING, range = "1-16", require = true, desc = "账号")
    @ApiDocParam(apiType = ApiType.REQ, param = "password", type = ApiParamType.STRING, range = "8-32", require = true, desc = "密码")
    @ApiDocParam(apiType = ApiType.RESP, param = "body", type = ApiParamType.OBJECT, range = "1", require = true, desc = "返回响应", objId = "LoginResp")
    @ApiDocParam(apiType = ApiType.RESP, param = "user", type = ApiParamType.OBJECT, range = "1", require = true, desc = "用户信息", objId = "User", refObjId = "LoginResp", objModel = SysUser.class)
    @ApiDocParam(apiType = ApiType.RESP, param = "menus", type = ApiParamType.OBJECT_ARRAY, range = "-", require = true, desc = "菜单", objId = "Menu", refObjId = "LoginResp", objModel = SysMenu.class)
    public ResponseMessage<LoginResp> login(LoginReq req) {
        return null;
    }
}

```

生成代码
```
public static void main(String[] args) throws Exception {
    boolean ret = ApiDocHandler.genApiDocModule("D:\\workspace\\temp\\apidocs", "com.yunyear", "java8-demo-1", "Java8示例", "用于演示Java8特性的代码");

    System.out.println(ret ? "生成成功" : "生成失败");
}
```

执行成功之后会在`D:\\workspace\\temp\\apidocs\\`文件中生成`java8-demo-1.json`文件，将此文件放置到`api-doc-web`指定的数据文件夹中，即可网站中查看此文档